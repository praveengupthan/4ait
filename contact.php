<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4A IT Services LLC</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>

    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="customContainer">
                <article>
                    <h1>Contact</h1>
                </article>
            </div>
        </div>
        <!--/ sub page header-->
        <!-- sub page body -->
        <div class="subpageBody">
           <div class="container">
               <!-- contact details row -->
               <div class="row justify-content-center">           
                <!-- col -->
                   <div class="col-sm-6 col-md-4 text-center mt-3 mt-md-5">
                        <div class="iconContact">
                            <span class="icon-telephone icomoon"></span>
                        </div>
                        <article class="pt-3">
                            <h4 class="fbold text-center">Phone</h4>
                            <p class="pb-0 text-center">+1-984-229-1774</p>                           
                        </article>
                   </div>
                   <!--/ col -->      
                    <!-- col -->
                   <div class="col-sm-6 col-md-4 text-center mt-3 mt-md-5">
                        <div class="iconContact">
                            <span class="icon-paperplane icomoon"></span>
                        </div>
                        <article class="pt-3">
                            <h4 class="fbold text-center">Mail us</h4>
                            <p class="pb-0 text-center">sales@4aitservices.com</p>                           
                        </article>
                   </div>
                   <!--/ col -->
                    <!-- col -->
                   <div class="col-sm-6 col-md-4 text-center mt-3 mt-md-5">
                        <div class="iconContact">
                            <span class="icon-pin icomoon"></span>
                        </div>
                        <article class="pt-3">
                            <h4 class="fbold text-center">Visit Us</h4>
                            <p class="pb-0 text-center">Durham, NC, USA & <br>Hyderabad, Telangana, India.</p>
                        </article>
                   </div>
                   <!--/ col -->                  
               </div>
               <!-- /.contact details row -->

               <!-- form row -->
               <div class="formRow row">
                   <div class="col-md-6 align-self-center">
                       <h4 class="subTitle">Reach us and Talk to our Expert</h4>
                       <p>We are always open 24/7 for you. Register Now!</p>
                       <?php     

if(isset($_POST['submitContact'])){
$to = "sales@4aitservices.com"; 
$subject = "Mail From ".$_POST['name'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['name']." has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>".$_POST['name']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['phone']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['email']."</td>
</tr>

<tr>
<th align='left'>Subject</th>
<td>".$_POST['sub']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['name']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);   

//success mesage
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?= $_POST['name'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>
                        <form id="contact_form" action="" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-floating mb-3 form-group">
                                            <input type="text" class="form-control" id="fullName" placeholder="Write Full Name" name="name">
                                            <label for="fullName">Write Full Name</label>
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 form-group">
                                            <input type="text" class="form-control" id="phoneNumber" placeholder="Phone Number" name="phone">
                                            <label for="phoneNumber">Phone Number</label>
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 form-group">
                                        <input type="text" class="form-control" id="emailAddress" placeholder="Email Address" name="email">
                                        <label for="emailAddress">Email Address</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating form-group">
                                        <textarea class="form-control" placeholder="Write Message" id="msg" style="height:100px;" name="msg"></textarea>
                                        <label for="msg">Write Message</label>
                                    </div>
                                </div>
                            </div>
                            <button class="btn redbtn w-100 mt-4" type="submit" name="submitContact">Submit</button>
                        </form>
                   </div>
                   <div class="col-md-6">
                       <img src="img/contactimg.png" alt="" class="img-fluid">
                   </div>
               </div>
               <!--/ form row -->
           </div>
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
   
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>