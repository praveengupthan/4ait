<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4A IT Services LLC</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>

    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="customContainer">
                <article>
                    <h1>Privacy Policy</h1>
                </article>
            </div>
        </div>
        <!--/ sub page header-->
        <!-- sub page body -->
        <div class="subpageBody">
          
        <!-- custom container -->
            <div class="customContainer">

            <p>4A IT Services LLC (“us,” “we,” or “our”) operates the http://www.4aitservices.com website (the “Service”).</p>
            <p>This page informs you of our policies regarding collecting, using, and disclosing personal data when you use our Service and the choices you have associated with that data.</p>
            <p>We use your data to provide and improve the Service. By using the Service, you agree to this policy's collection and use of information. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as our Terms and Conditions, accessible from http://www.4aitservices.com</p>
            <h5 class="fbold">Information Collection and Use</h5>   
            <p>We collect several different types of information to provide and improve our Service to you.</p>           
            <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you (“Personal Data”). Personally identifiable information may include but is not limited to email address, full name, phone number, work address, zip code, and city.</p>
            <p>We may also collect information on how the Service is accessed and used (“Usage Data”). This Usage Data may include information such as your computer’s Internet Protocol address (e.g., IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>
            <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>
            <p>Cookies are files with a small amount of data that may include a unique anonymous identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and improve and analyze our Service.</p>
            <p>You can instruct your browser to refuse all cookies or indicate when a cookie is sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>

             <h5 class="fbold">Use of Data</h5>   
             <p class="fbold">4A IT Services LLC uses the collected data for various purposes:</p>
             <ul class="listItems pb-4">
                 <li>To provide and maintain the Service</li>
                 <li>To notify you about changes to our Service</li>
                 <li>To allow you to participate in interactive features of our Service when you choose to do so</li>
                 <li>To provide customer care and support</li>
                 <li>To provide analysis or valuable information so that we can improve the Service</li>
                 <li>To monitor the usage of the Service</li>
                 <li>To detect, prevent and address technical issues</li>
                 <li>Disclosure Of Data</li>
             </ul>

             <p class="fbold">4A IT Services LLC Solutions may disclose your Data in the good faith belief that such action is necessary to:</p>

             <ul class="listItems pb-4">
                <li>To comply with a legal obligation</li>
                <li>To protect and defend the rights or property of 4A IT Services Solutions</li>
                <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                <li>To protect the personal safety of users of the Service or the public</li>
                <li>To protect against legal liability</li>
                <li>Security Of Data</li>
             </ul>

             <p>4A IT Services will take all steps reasonably necessary to ensure that your data is treated securely and by this Privacy Policy, and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place, including the security of your data and other personal information. The security of your data is important to us but remember that no method of transmission over the Internet or electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>

             <h5 class="fbold">Changes To This Privacy Policy</h5>   
             <p>4A IT Services may update our Privacy Policy from time to time based on customer feedback. You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when posted on this page.</p>

              <h5 class="fbold">Contact Information</h5>   
             <p>If you have any questions about this Privacy Policy, please contact us at hr@4aitservices.com</p>

            </div>
            <!-- /custom container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->   
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>