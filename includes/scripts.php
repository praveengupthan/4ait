 <!-- scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/custom.js"></script>

    <script>    
      //contact form validatin
      $('#contact_form').validate({
         ignore: [],
         errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
         errorElement: 'div',
         errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
         },
         highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
         },  
         rules: {
            name: {
                  required: true                
            },
            phone: {
                  required: true,    
                  number: true,
                  minlength: 10,
                  maxlength:10                   
            },  
            email:{
               required:true,
               email: true
            },           
         },

         messages: {
            name: {
                  required: "Enter Name"
            },
            phone: {
                  required: "Enter Valid Mobile Number"                  
            }, 
            email:{
               required: "Enter Valid Email"        
            },               
         },
      });
   </script>