 <!-- header -->
 <div id="load"><img src="img/DzUd.gif"></div>      
 <header class="fixed-top">
        <div class="customContainer">
            <nav class="navbar navbar-expand-lg navbar-light ">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">
                        <img src="img/logo.svg" alt="4A IT Consulting Services">
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" aria-current="page" href="index.php">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='company.php'){echo'active';}else {echo'nav-link';}?>" href="company.php">Company</a>
                            </li>
                            <li class="nav-item">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='services.php'){echo'active';}else {echo'nav-link';}?>" href="services.php">Software Consulting</a>
                            </li>
                            <li class="nav-item">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='staffaugumentation.php'){echo'active';}else {echo'nav-link';}?>" href="staffaugumentation.php">Staff Augmentation</a>
                            </li>
                             <li class="nav-item">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='industries.php'){echo'active';}else {echo'nav-link';}?>" href="industries.php">Industries</a>
                            </li>
                            <li class="nav-item">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php">Contact</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#headeroffcanvas" data-bs-toggle="offcanvas" role="button"
                                    aria-controls="headeroffcanvas"><span class="icon-burgerbar icomoon"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!-- off canvas -->
    <div class="offcanvas offcanvas-end" tabindex="-1" id="headeroffcanvas" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasExampleLabel">4A IT Services LLC</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <div>       
                <article>
                    <p class="pb-3">We have an uncompromising determination to achieve excellence in everything we do. We offer our partners unmatched quality, streamlined execution, responsiveness, and flexibility that go above and beyond traditional staffing services. We know even the best can always be better. When faced with the choice, we would rather be the best than the biggest. Discipline differentiates us.</p>

                    <a class="redbtn linkbtn" href="company.php">Read More</a>
                </article>         
               <table class="headertable table-borderless mt-3">    
                    <tr>
                       <td><span class="icon-telephone icomoon"></span></td>
                       <td>+1-984-229-1774</td>
                    </tr>              
                    <tr>
                       <td><span class="icon-email icomoon"></span></td>
                       <td>sales@4aitservices.com</td>
                    </tr>
                     <tr>
                       <td><span class="icon-pin icomoon"></span></td>
                       <td>Durham, NC, USA & <br>Hyderabad, Telangana, India.
                    </td>
                    </tr>
               </table>

               <div class="py-4 border-top mt-3 headerSocial">
                    <a href="https://www.facebook.com/4A-It-Services-LLC-110542721612519" target="_blank"><span class="icon-facebook icomoon"></span></a>
                    <a href="https://www.linkedin.com/company/4aitservices" target="_blank"><span class="icon-linkedin icomoon"></span></a>                   
               </div>
            </div>
        </div>
    </div>
    <!--/ off canvas -->
    <!--/ header -->