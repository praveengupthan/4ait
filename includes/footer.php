 <!-- footer -->
    <footer>
        <a href="javascript:void(0)" class="moveTop" id="movetop"><span class="icon-uparrow icomoon"></span></a>
        <!-- container -->
        <div class="customContainer">
            <div class="row pb-3 pb-md-5">
                <div class="col-md-4">
                    <h4>About 4A IT Services LLC</h4>
                    <p>Our specialists build talent pools of qualified candidates who want to work for you. We use your brand to attract the right talent informed by proven advertising and recruitment best practices. People are our product, and all people have both strengths and weaknesses which need to be considered.
                    </p>                 
                   
                    <a href="company.php" class="d-inline-block py-3 readmoreLink">Read More <span class="icon-rightarrowlong icomoon"></span></a>
                   
                </div>
                <div class="col-md-3">
                    <h4>Quick Links</h4>
                    <ul class="footerLinks">
                        <li><a href="index.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'activeLink';}else {echo'nav-link';}?>">Home</a></li>
                        <li><a href="company.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='company.php'){echo'activeLink';}else {echo'nav-link';}?>">Company</a></li>
                        <li><a href="services.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='services.php'){echo'activeLink';}else {echo'nav-link';}?>">Software Consulting</a></li>
                        <li><a href="staffaugumentation.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='staffaugumentation.php'){echo'activeLink';}else {echo'nav-link';}?>">Staff Augmentation</a></li>
                        <li><a href="industries.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='industries.php'){echo'activeLink';}else {echo'nav-link';}?>">Industries</a></li>
                        <li><a href="contact.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'activeLink';}else {echo'nav-link';}?>">Contact</a></li>                  
                        <li><a href="privacypolicy.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='privacypolicy.php'){echo'activeLink';}else {echo'nav-link';}?>">Privacy Policy</a></li>                  
                    </ul>
                </div>
                <div class="col-md-5">
                    <h4>Reach Us</h4>
                    <article>
                        <p><span class="icon-telephone icomoon"></span> +1-984-229-1774<</p>        
                        <p><span class="icon-email icomoon"></span> sales@4aitservices.com</p>                        
                        <p><span class="icon-pin icomoon"></span>Durham, NC, USA & <br>Hyderabad, Telangana, India.</p>
                    </article>
                </div>
            </div>
        </div>
        <!--/ container -->
        <p class="rights">© All Rights Reserved 2022</p>
    </footer>
    <!--/ footer -->