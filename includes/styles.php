<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<!-- styles -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/swiper.css">
<link rel="stylesheet" href="css/icomoon.css">