<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4A IT Services LLC</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>

    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="customContainer">
                <article>
                    <h1>Company</h1>
                </article>
            </div>
        </div>
        <!--/ sub page header-->
        <!-- sub page body -->
        <div class="subpageBody">
            <div class="customContainer">
                <div class="row pb-3">
                    <div class="col-md-7 align-self-center">
                        <article class="pb-3">
                            <h6>About Company</h6>
                            <h2>Businesses & Processes Redefined</h2>
                            <h4><i>The client’s business is handled with utmost diligence. Consider us an extension of your talent acquisition team.  </i></h4>
                        </article>
                        <p>Our specialists build talent pools of qualified candidates who want to work for you.  We use your brand to attract the right talent informed by proven advertising and recruitment best practices. People are our product, and all people have both strengths and weaknesses which need to be considered. We will always present a balanced appraisal of our consultants to ensure the success of both our customers and consultants. </p>
                        <p>We have an uncompromising determination to achieve excellence in everything we do. We offer our partners unmatched quality, streamlined execution, responsiveness, and flexibility that go above and beyond traditional staffing services. We know even the best can always be better. When faced with the choice, we would rather be the best than the biggest. Discipline differentiates us.</p>
                        <p>The first step to having a great organization is to have the right people. Specifically, the right people for that organization. since every organization is unique. We understand that determining the right people goes beyond identifying specific knowledge and skills; and that an individual’s character, values, and innate abilities ultimately determine success. We make an unusual effort to identify and recruit the best person for each job for our clients and ourselves.</p>

                        <div class="d-flex pt-3 pt-md-4">
                            <div class="aboutcol">
                                <span class="icon-badge icomoon"></span>
                            </div>
                            <div class="aboutcol ps-2">
                                <h4 class="fbold">Experience</h4>
                                <p>Our leadership team of more than two decades of experience.</p>
                            </div>
                        </div>
                        <div class="d-flex pt-3 pt-md-4">
                            <div class="aboutcol">
                                <span class="icon-qtechnicalsupporty icomoon"></span>
                            </div>
                            <div class="aboutcol ps-2">
                                <h4 class="fbold">Quick Support</h4>
                                <p>We’ll help you test bold new ideas.</p>
                            </div>
                        </div>
                    </div>

                    <!-- img -->
                    <div class="col-md-5">
                        <img src="img/aboutimg01.png" alt="" class="img-fluid w-100">
                    </div>
                    <!--/ img -->
                </div>
            </div>
            <!-- our process -->
            <div class="cardSection">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <article class="text-center">
                                <p class="fred text-uppercase fbold text-center">Our Process</p>
                                <h3 class="subTitle">Driving Client Results Utilizing New Innovation Points of view</h3>
                            </article>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row pt-3 pt-md-5">
                        <div class="col-lg-6 mb-3">
                            <div class="processCol">
                                <img src="img/assess.jpg" alt="" class="img-fluid w-100 mb-3">
                                <div class="d-flex">
                                    <div class="processNumber">
                                        1
                                    </div>
                                    <div class="ps-3 ps-md-4 processarticle align-self-center">
                                        <h4 class="fbold">Assess</h4>
                                    </div>
                                </div>
                                <p class="pt-4">We have the core expertise to assess; if the assessment is needed & to what extent it’s needed. Our assessment models run across the functionalities of the complete business requirement and are agnostic. The assessment phase is where the exact data points are blended. The assessing cycle travels to the next step of analysing.</p>
                            </div>
                        </div>
                         <div class="col-lg-6 mb-3">
                            <div class="processCol">
                                <img src="img/analyse.jpg" alt="" class="img-fluid w-100 mb-3">
                                <div class="d-flex">
                                    <div class="processNumber">
                                        2
                                    </div>
                                    <div class="ps-3 ps-md-4 processarticle align-self-center">
                                        <h4 class="fbold">Analyse</h4>
                                    </div>
                                </div>
                                <p class="pt-4">We analyse what the customer calls for with the assessment plan in place with the demand & need for catering to an opportunity—running thru the cycle of needs & building a perfect solution of the assessed business model—having a bullet & fool proof confluence of all the elements to deliver the right answer. The next step travels to the aligning square.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="processCol">
                                <img src="img/align.jpg" alt="" class="img-fluid w-100 mb-3">
                                <div class="d-flex">
                                    <div class="processNumber">
                                        3
                                    </div>
                                    <div class="ps-3 ps-md-4 processarticle align-self-center">
                                        <h4 class="fbold">Align</h4>
                                    </div>
                                </div>
                                <p class="pt-4">We specialize in constructing a proper model of solutions saving time, effort & money for the client by aligning with the right team. The teams are segregated as per the strength & efficacy of deliveries—the perfect equation for SWOT. We are betting on the right resources for proper delivery to act.</p>
                            </div>
                        </div>
                         <div class="col-lg-6 mb-3">
                            <div class="processCol">
                                <img src="img/act.jpg" alt="" class="img-fluid w-100 mb-3">
                                <div class="d-flex">
                                    <div class="processNumber">
                                        4
                                    </div>
                                    <div class="ps-3 ps-md-4 processarticle align-self-center">
                                        <h4 class="fbold">Act</h4>
                                    </div>
                                </div>
                                <p class="pt-4">The engine fires, filtering & cluttering all the unwanted data in the initial 3 phases. The system runs on feasible timelines, costs, hassle-free delivery, quality, sanity, proposals & completion. The action phase ends with the exact binding solution on the client’s table.</p>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
            </div>
            <!--/ our process -->
           
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
   
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>