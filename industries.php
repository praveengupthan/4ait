<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Software Consulting & Services</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>

    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="customContainer">
                <article>
                    <h1>Industries</h1>
                </article>
            </div>
        </div>
        <!--/ sub page header-->
        <!-- sub page body -->
        <div class="subpageBody">
            <div class="customContainer">
                 <!-- row -->        
                <div class="row pb-3 pb-lg-5">
                    <div class="col-md-6">
                        <img src="img/industryvertical.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">  
                        <h4 class="subTitle">Industry Verticals</h4>
                        <p>We provide Recruitment and Staffing services to many industries and domains through our innovative and customized solutions and passionate commitment to research. Understanding the hiring strategies, availability of talent, and compensation benchmarking makes us a proud hiring partner for various industries.</p>
                        <ul class="listItems">
                            <li>Information Technology</li>
                            <li>Banking and Financial Services</li>
                            <li>Pharmaceutical Life Sciences and Healthcare</li>
                            <li>Infrastructure</li>
                            <li>Automobile</li>
                            <li>Digital</li>
                            <li>Engineering</li>
                            <li>Research and Consulting</li>
                        </ul>
                    </div>                    
                </div>
                 <!--/ row -->

                <!-- row -->        
                <div class="row pb-3 pb-lg-5">
                    <div class="col-md-6 order-lg-last">
                        <img src="img/functionalvertical.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">  
                        <h4 class="subTitle">Functional Verticals</h4>
                        <p>In the growing business market where highly skilled and specialized resources are required, we at V3 Staffing provide experienced resources from the specific functional vertical through our unique recruitment approach.</p>
                        <ul class="listItems">
                            <li>Finance and Accounting</li>
                            <li>Sales and Marketing</li>
                            <li>Legal</li>
                            <li>Human Resource</li>
                            <li>Operations and Supply Chain</li>
                            <li>Administration</li>
                        </ul>
                    </div>                    
                </div>
                 <!--/ row -->              

                <!-- row -->        
                <div class="row pb-3 pb-lg-5">
                    <div class="col-md-6">
                        <img src="img/ourniche.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">  
                        <h4 class="subTitle">Our Niche</h4>
                        <p>Perseverance, Persistence, Accountability, Commitment, Service, Straight Talk, Integrity, Clarity of thought, Open-Mindedness, and being Neighbourly.</p>
                         <ul class="listItems">
                            <li>Gig sectors</li>
                            <li>High accelerated hiring</li>
                            <li>Advanced crowd Sourcing</li>
                            <li>Long term relationships</li>
                            <li>Tech sector expertise</li>
                            <li>Deep Agility</li>
                            <li>Adaptive recruitment solutions </li>
                        </ul>                      
                    </div>                    
                </div>
                 <!--/ row -->


                
            </div>

         

        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
   
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>