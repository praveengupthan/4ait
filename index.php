<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4A IT Services LLC</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main>
        <!-- slider -->
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active slide01">
                    <div class="container-fluid gx-0">
                        <div class="row justify-content-end">
                            <div class="col-md-6 article">
                                <div class="sliderContent ps-2 ps-md-5">
                                    <h1 class="sliderTitle">Riding the recruitment <span>tide</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item slide02">
                    <div class="container-fluid gx-0">
                        <div class="row justify-content-end">
                            <div class="col-md-6 article">
                                <div class="sliderContent ps-2 ps-md-5">
                                    <h2 class="sliderTitle">Chasing potential <span>talent</span> </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item slide03">
                    <div class="container-fluid gx-0">
                        <div class="row justify-content-end">
                            <div class="col-md-6 article">
                                <div class="sliderContent ps-2 ps-md-5">
                                    <h2 class="sliderTitle">Hiring quality <span>Workforce</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                <span class="icon-arrowleft  icomoon"></span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                <span class="icon-rightarrowlong icomoon"></span>
            </button>
        </div>
        <!--/ slider -->
        <!-- slider cards -->
        <section class="sliderCards">

            <div class="container-fluid">
                <!-- Swiper -->
                <div class="swiper-container sliderCardsContainer">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="cardSliderBox">
                                <h3>Develop and maintain technology solutions</h3>
                                <p class="middlepara">We at 4A Develop and maintain technology solutions to provide businesses a new channel for customer relationships, online services, and content management.</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="cardSliderBox">
                                <h3>Set mutual goals and objectives</h3>
                                <p class="middlepara">Set mutual goals and objectives resulting in client satisfaction through timely execution of strategic proprieties and provides strategic software solutions for business needs.</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="cardSliderBox">
                                <h3>Clients</h3>
                                <p class="middlepara">We help clients plan, design, and deploy projects that give them unparalleled technical competitiveness and help clients reach short- and long-term goals.</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="cardSliderBox">
                                <h3>Technology & Staffing Solutions</h3>
                                <p class="middlepara">4A has proven technology & staffing solutions for industry-specific business needs in the insurance, public sector, manufacturing, energy, banking, financial, and retail sectors.</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="cardSliderBox">
                                <h3>IT Professionals</h3>
                                <p class="middlepara">We deploy talented IT Professionals with exceptional skills and can deliver results and satisfaction to clients of various technology needs.</p>
                            </div>
                        </div>                   
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>               
            </div>
        </section>
        <!--/ slider cards -->
        <!-- clients -->
        <section class="clientsHome d-none">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <p><small class="text-uppercase fbold fred">KEY DIFFERENTIATORS</small></p>
                        <h4 class="subTitle">We Creating Solutions for your Organization</h4>
                    </div>
                    <div class="col-md-8">
                        <p class="pt-3">4A is the “Brainchild’ of the team whose “alter ego” has had always been in IT
                            Consulting, User friendly project demonstration, customer success & building friendly
                            successful business models for fortune 1000 clients & start-ups.</p>
                    </div>
                </div>
                <!-- logos -->
                <div class="logosSlider">
                    <!-- Swiper -->
                    <div class="swiper-container clientsSwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide"><img src="img/clients/1.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/2.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/3.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/4.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/1.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/2.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/3.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/4.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/1.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/2.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/3.png" alt=""></div>
                            <div class="swiper-slide"><img src="img/clients/4.png" alt=""></div>
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination"></div> -->
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                </div>
                <!--/ logos -->
            </div>
        </section>
        <!--/ clients -->
        <!-- about section -->
        <section class="aboutHome">
            <div class="container-fluid gx-0">
                <div class="row gx-0">
                    <div class="col-md-8 aboutLeftCol">
                        <h6 class="text-uppercase">About 4A IT Services LLC</h6>
                        <h4 class="subTitle">4A is a global IT consultancy that seamlessly integrates experts and leading-edge solutions into your organization so you can focus on what matters.</h4>
                        <h6 class="text-uppercase">Key differentiators</h6>
                        <p><i>4A is the “Brainchild’ of the team whose “alter ego” has had always been in; IT Consulting, User-friendly project demonstration, customer success & building close successful business models for fortune 1000 clients & start-ups. The founding team;</i></p>
                         <ul class="listItems pb-3">
                             <li>Possess two decades of experience across Giant & SMB enterprises, ensuring the development of companies handling multi $M businesses</li>
                             <li>Successfully recruited over 10000 professionals across US/UK & APAC working with various organizations directly/indirectly.</li>
                             <li>Handled the biggest of challenges thru market downfalls/crashes without compromising in the quality, time, costs & business continuity working with the clients</li>
                             <li>Having expertise working with the best greenfield & passive methodologies collaborating with Fortune 500 companies.</li>
                             <li>Are deft IT evangelists who participated in handling services & solutions starting way back from Hardware to the recent Cloud models upgrading with the current & futuristic technologies & demonstrating them across successfully.</li>
                         </ul>
                        <a href="javascript:void(0)" class="linkbtn redbtn">Read More About us</a>
                    </div>
                    <div class="col-md-6 aboutImg">
                        <img src="img/abouthome.jpg" alt="" class="img-fluid w-100">
                    </div>
                </div>
            </div>
        </section>
        <!--/ about section -->
        <!-- services -->
        <section class="services d-none">
            <div class="container">
                <div class="row justify-content-center py-3">
                    <div class="col-md-5">
                        <article class="text-center">
                            <p class="fred text-uppercase fbold">Our Services</p>
                            <h4 class="subTitle">We run all kinds of services in form of Information & Technologies</h4>
                        </article>
                    </div>
                </div>
                <!-- swiper -->
                <div class="servicesHome">
                    <!-- Swiper -->
                    <div class="swiper-container servicesHomeContainer">
                        <div class="swiper-wrapper">
                            <!-- item -->
                            <div class="swiper-slide">
                                <div class="serviceItem">
                                    <img src="img/service01.jpg" alt="" class="img-fluid">
                                    <article class="d-flex py-3 justify-content-between">
                                        <div class="serviceIcon">
                                            <span class="icon-customerservice icomoon"></span>
                                        </div>
                                        <div class="ps-2 serviceArticle">
                                            <h5>Web Development</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the eaque</p>
                                            <a href="javascript:void(0)">Read More <span
                                                    class="icon-rightarrowlong icomoon"></span></a>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="swiper-slide">
                                <div class="serviceItem">
                                    <article class="d-flex py-3 justify-content-between">
                                        <div class="serviceIcon">
                                            <span class="icon-customerservice icomoon"></span>
                                        </div>
                                        <div class="ps-2 serviceArticle">
                                            <h5>Backend Development</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the eaque</p>
                                            <a href="javascript:void(0)">Read More <span
                                                    class="icon-rightarrowlong icomoon"></span></a>
                                        </div>
                                    </article>
                                    <img src="img/service02.jpg" alt="" class="img-fluid">
                                </div>
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="swiper-slide">
                                <div class="serviceItem">
                                    <img src="img/service03.jpg" alt="" class="img-fluid">
                                    <article class="d-flex py-3 justify-content-between">
                                        <div class="serviceIcon">
                                            <span class="icon-customerservice icomoon"></span>
                                        </div>
                                        <div class="ps-2 serviceArticle">
                                            <h5>Mobile Application</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the eaque</p>
                                            <a href="javascript:void(0)">Read More <span
                                                    class="icon-rightarrowlong icomoon"></span></a>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="swiper-slide">
                                <div class="serviceItem">
                                    <article class="d-flex py-3 justify-content-between">
                                        <div class="serviceIcon">
                                            <span class="icon-customerservice icomoon"></span>
                                        </div>
                                        <div class="ps-2 serviceArticle">
                                            <h5>Web Development</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the eaque</p>
                                            <a href="javascript:void(0)">Read More <span
                                                    class="icon-rightarrowlong icomoon"></span></a>
                                        </div>
                                    </article>
                                    <img src="img/service01.jpg" alt="" class="img-fluid">
                                </div>
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="swiper-slide">
                                <div class="serviceItem">
                                    <img src="img/service02.jpg" alt="" class="img-fluid">
                                    <article class="d-flex py-3 justify-content-between">
                                        <div class="serviceIcon">
                                            <span class="icon-customerservice icomoon"></span>
                                        </div>
                                        <div class="ps-2 serviceArticle">
                                            <h5>Backend Development</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the eaque</p>
                                            <a href="javascript:void(0)">Read More <span
                                                    class="icon-rightarrowlong icomoon"></span></a>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="swiper-slide">
                                <div class="serviceItem">
                                    <article class="d-flex py-3 justify-content-between">
                                        <div class="serviceIcon">
                                            <span class="icon-customerservice icomoon"></span>
                                        </div>
                                        <div class="ps-2 serviceArticle">
                                            <h5>Mobile Development</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the eaque</p>
                                            <a href="javascript:void(0)">Read More <span
                                                    class="icon-rightarrowlong icomoon"></span></a>
                                        </div>
                                    </article>
                                    <img src="img/service03.jpg" alt="" class="img-fluid">
                                </div>
                            </div>
                            <!--/ item -->
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <!--/ swiper -->
            </div>
        </section>
        <!--/ services -->
        <!-- industries -->
        <div class="homeIndustries position-relative pb-3 pb-md-5">
            <div class="container">
                <article class="text-center py-3 py-md-5">
                    <p class="fbold">Whether it’s building a rock-solid team, finding the right fit for your organization, or developing solutions, we’re in it with you. Let’s work together.</p>
                    <h4 class="subTitle">We excel in our industry so that you can get in yours.</h4>
                </article>
                <div class="industriesrow">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-informationtechnology icomoon"></span>
                                </div>
                                <h6>Information Technology</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-bank icomoon"></span>
                                </div>
                                <h6>Banking and Financial Services</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-healthcare icomoon"></span>
                                </div>
                                <h6>Pharmaceutical Life Sciences and Healthcare</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-operating icomoon"></span>
                                </div>
                                <h6>Infrastructure</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-automobile icomoon"></span>
                                </div>
                                <h6>Automobile</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-digital icomoon"></span>
                                </div>
                                <h6>Digital</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-engineering icomoon"></span>
                                </div>
                                <h6>Engineering</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-research-1 icomoon"></span>
                                </div>
                                <h6>Research and Consulting</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-accounting icomoon"></span>
                                </div>
                                <h6>Finance and Accounting</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-promotion icomoon"></span>
                                </div>
                                <h6>Sales and Marketing</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-auction icomoon"></span>
                                </div>
                                <h6>Legal</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-steam icomoon"></span>
                                </div>
                                <h6>Human Resource</h6>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-testing icomoon"></span>
                                </div>
                                <h6>Operations and Supply Chain</h6>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-2 col-md-4 col-sm-3 col-6 text-center">
                            <div class="indCol p-3">
                                <div class="industriesIcon">
                                    <span class="icon-administrator icomoon"></span>
                                </div>
                                <h6>Administration</h6>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </div>
        </div>
        <!--/ industries -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>